package kz.post.game.game.repository;

import kz.post.game.game.model.AnswerEntity;
import kz.post.game.game.model.AnswerKzEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AnswerKzRepository extends JpaRepository<AnswerKzEntity,Long>{
    List<AnswerKzEntity> findAllByQuestionId(Long question);
}
