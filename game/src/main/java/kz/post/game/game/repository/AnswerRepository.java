package kz.post.game.game.repository;

import kz.post.game.game.model.AnswerEntity;
import kz.post.game.game.model.QuestionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AnswerRepository extends JpaRepository<AnswerEntity, Long>{
    List<AnswerEntity> findAllByQuestionId(Long question);
}
