package kz.post.game.game.repository;

import kz.post.game.game.model.GameEntity;
import kz.post.game.game.model.UserEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface GameRepository extends JpaRepository<GameEntity, Long>{

    List<GameEntity> findAllByUser(UserEntity userEntity);

    @Transactional
    @Modifying
    @Query("UPDATE GameEntity g SET g.scores = ?1 where g.id = ?2")
    void updateFinishedGame(Integer score, Long id);

    @Query(value = "SELECT g FROM GameEntity g where g.scores is not null order by g.scores desc")
    List<GameEntity> getAllScores(Pageable pageable);


}
