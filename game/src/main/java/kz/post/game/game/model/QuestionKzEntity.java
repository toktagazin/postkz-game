package kz.post.game.game.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "t_game_kz_question")
public class QuestionKzEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String question;

    private Long answer_id;

    @ElementCollection
    @CollectionTable
    private List<AnswerKzEntity> answers;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Long getAnswer_id() {
        return answer_id;
    }

    public void setAnswer_id(Long answer_id) {
        this.answer_id = answer_id;
    }


    public List<AnswerKzEntity> getAnswers() {
        return answers;
    }

    public void setAnswers(List<AnswerKzEntity> answers) {
        this.answers = answers;
    }
}
