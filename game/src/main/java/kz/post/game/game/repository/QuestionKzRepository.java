package kz.post.game.game.repository;

import kz.post.game.game.model.QuestionKzEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionKzRepository extends JpaRepository<QuestionKzEntity, Long>{
}
