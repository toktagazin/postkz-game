package kz.post.game.game.model;

import javax.persistence.*;
import java.util.List;

@Table(name = "t_games")
@Entity
public class GameEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;

    private Integer scores;

    @Column(name = "game_seq")
    private Integer gameSeq;


    @ElementCollection
    @CollectionTable
    private List<QuestionEntity> questions;

    @ElementCollection
    @CollectionTable
    private List<QuestionKzEntity> kzQuestions;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    public Integer getScores() {
        return scores;
    }

    public void setScores(Integer scores) {
        this.scores = scores;
    }

    public Integer getGameSeq() {
        return gameSeq;
    }

    public void setGameSeq(Integer gameSeq) {
        this.gameSeq = gameSeq;
    }

    public List<QuestionEntity> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionEntity> questions) {
        this.questions = questions;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public List<QuestionKzEntity> getKzQuestions() {
        return kzQuestions;
    }

    public void setKzQuestions(List<QuestionKzEntity> kzQuestions) {
        this.kzQuestions = kzQuestions;
    }
}
