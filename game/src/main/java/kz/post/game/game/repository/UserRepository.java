package kz.post.game.game.repository;

import kz.post.game.game.model.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface UserRepository extends JpaRepository<UserEntity, Long> {
    UserEntity findByLogin(String login);

    @Transactional
    @Modifying
    @Query("UPDATE UserEntity u SET u.phone = ?1 where u.login = ?2")
    void updateUserInfo(String phone, String login);
}
