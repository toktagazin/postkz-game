package kz.post.game.game.rest;

import kz.post.game.game.model.*;
import kz.post.game.game.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.web.bind.annotation.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@RestController
@CrossOrigin
@RequestMapping(value = "/api")
public class GameResource {

    @Autowired
    private QuestionRepository questionRepository;
    @Autowired
    private QuestionKzRepository questionKzRepository;
    @Autowired
    private GameRepository gameRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AnswerRepository answerRepository;
    @Autowired
    private AnswerKzRepository answerKzRepository;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @RequestMapping(value = "/startGame", method = RequestMethod.GET)
    public Map<String, Object> startGame(@RequestParam(value = "login") String login, @RequestParam(value = "phone", required = false) String phone,
                                @RequestParam(value = "lang") String lang){
        List<QuestionEntity> questionEntityList = new ArrayList<>();
        List<QuestionKzEntity> questionKzEntityList = new ArrayList<>();
        List<QuestionEntity> allQuestions = questionRepository.findAll();
        List<QuestionKzEntity> allQuestionsKz = questionKzRepository.findAll();

        List<Integer> questSeq = new ArrayList<>();
        for(int i = 0; i < 10;){
            int rnd = new Random().nextInt(allQuestions.size());
            if(!questSeq.contains(rnd)){
                questSeq.add(rnd);
                i++;
            }
        }
        List<Integer> questSeqKz = new ArrayList<>();
        for(int i = 0; i < 10;){
            int rnd = new Random().nextInt(allQuestionsKz.size());
            if(!questSeqKz.contains(rnd)){
                questSeqKz.add(rnd);
                i++;
            }
        }
        if(lang.equals("ru")){
            for(Integer seqNum: questSeq){
                QuestionEntity questionEntity = allQuestions.get(seqNum);
                List<AnswerEntity> answerEntities = answerRepository.findAllByQuestionId(questionEntity.getId());
                questionEntity.setAnswers(answerEntities);
                questionEntityList.add(questionEntity);
            }
        }else {
            for(Integer seqNum: questSeqKz){
                QuestionKzEntity questionEntity = allQuestionsKz.get(seqNum);
                List<AnswerKzEntity> answerEntities = answerKzRepository.findAllByQuestionId(questionEntity.getId());
                questionEntity.setAnswers(answerEntities);
                questionKzEntityList.add(questionEntity);
            }
        }



        UserEntity user = userRepository.findByLogin(login);
        if(user == null){
            user = new UserEntity();
            user.setLogin(login);
            user.setPhone(phone);
            userRepository.saveAndFlush(user);
        }else {
            userRepository.updateUserInfo(phone, login);
            user.setPhone(phone);
        }

        Integer games = jdbcTemplate.queryForObject("select count(*) from t_games where user_id =?", Integer.class,user.getId());

        GameEntity gameEntity = new GameEntity();
        gameEntity.setUser(user);
        gameEntity.setGameSeq(games);
        if(lang.equals("ru")){
            gameEntity.setQuestions(questionEntityList);
        }else {
            gameEntity.setKzQuestions(questionKzEntityList);
        }
        gameRepository.save(gameEntity);
        Map<String, Object> result = new HashMap<>();
        result.put("id", gameEntity.getId());
        result.put("user", gameEntity.getUser());
        result.put("scores", gameEntity.getScores());
        result.put("gameSeq", gameEntity.getGameSeq());
        if(lang.equals("ru")){
            result.put("questions",questionEntityList);
        }else{
            result.put("questions", questionKzEntityList);
        }
        return result;
    }

    @RequestMapping(value = "/finishGame")
    public void finishGame(@RequestParam(name = "score") Integer score, @RequestParam(name = "id") Long id){
        gameRepository.updateFinishedGame(score,id);
    }

    @RequestMapping(value = "/getScores")
    public List<GameEntity> getScores(){
        return gameRepository.getAllScores(new PageRequest(0,5));

    }

}
